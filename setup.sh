#!/usr/bin/bash

set -euxo pipefail

rm -rf dotfiles || true

#
# Install some Apt stuff
#

apt update
apt install -y git neovim curl python3.9

#
# Install Vim Plug
#

curl -fLo ~/.vim/autoload/plug.vim --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

# 
# Clone my dotfiles
#

git clone https://github.com/dkull/dotfiles.git
cp dotfiles/.* ~/ || true

#
# VIM setup
#

mkdir -p ~/.config/nvim/

echo "set runtimepath^=~/.vim runtimepath+=~/.vim/after" >> ~/.config/nvim/init.vim
echo "let &packpath=&runtimepath" >> ~/.config/nvim/init.vim
echo "source ~/.vimrc" >> ~/.config/nvim/init.vim

vim -c PlugInstall -c q -c q

